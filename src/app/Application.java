package app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Application {

	public static void main(String[] args) throws ParseException {
		IAppareil appareil=new Appareil();
		appareil.enregistrer(new Contact(1, "C1", "07221123"));
		appareil.enregistrer(new Contact(2, "C2", "07221456"));
		appareil.enregistrer(new AppelEmi(1, new Date(), 66), "07221123");
		appareil.enregistrer(new AppelEmi(2, new Date(), 125), "07221123");
		appareil.enregistrer(new AppelEmi(3, new Date(), 122), "07221123");
		appareil.enregistrer(new AppelRecu(4, new Date(), 98), "07221456");
		System.out.println("---------------------------------------");
		System.out.println("Consulter un contact sachant son numero");
		try {
			Contact c=appareil.consulter(1);
			System.out.println("Num= "+c.getNumero());
			System.out.println("Nom : "+c.getNom());
			System.out.println("Tel : "+c.getNumeroTel());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("---------------------------------------");
		System.out.println("Consulter les contacts par mot clef:");
		List<Contact> contacts=appareil.consulter("C");
		for(Contact c:contacts) {
			System.out.println("***********************************");
			System.out.println("Nom :"+c.getNom());
			System.out.println("Tel :"+c.getNumeroTel());
		}
		
		System.out.println("---------------------------------------");
		System.out.println("Consulter le cout total des appels:");
		System.out.println("Cout total:"+appareil.coutTotalAppels());
		
		System.out.println("---------------------------------------");
		System.out.println("Consulter le cout appels d'un contact:");
		System.out.println("Cout :"+appareil.coutAppels(1));
		
		System.out.println("---------------------------------------");
		System.out.println("Consulter le cout appels entre deux dates:");
		SimpleDateFormat sdf=new SimpleDateFormat("dd/mm/yyyy");
		Date d1=sdf.parse("11/12/2014");
		Date d2=sdf.parse("12/12/2020");
		System.out.println("Cout :"+appareil.coutAppels(d1, d2));
	}
} 

